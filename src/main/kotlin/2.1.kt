import java.util.Scanner

/*
* AUTHOR: David Moreno Fernández
* DATE: 16/12/2022
* TITLE: Factorial
*/

fun main (){
    val scanner= Scanner(System.`in`)
    val number= scanner.nextInt()
    println(factorial(number))
}

fun factorial (number: Int):Long{
    return if(number==1) number.toLong()
    else if (number==0) number+1.toLong()
    else number * factorial(number-1)
}