import java.util.*

/*
* AUTHOR: David Moreno Fernández
* DATE: 16/12/2022
* TITLE: Doble factorial
*/

fun main(){
    val scanner= Scanner(System.`in`)
    val number= scanner.nextInt()
    println(doubleFactorial(number))
}

fun doubleFactorial(number: Int): Long{
    return if(number==1) number.toLong()
    else if (number==2) number.toLong()
    else if (number==0) number+1.toLong()
    else number * doubleFactorial(number-2)
}
