import java.util.*
import kotlin.properties.Delegates

/*
* AUTHOR: David Moreno Fernández
* DATE: 08/01/2023
* TITLE: Nombre de dígits
*/

fun main() {
    val scanner = Scanner(System.`in`)
    var number = scanner.nextInt()
    println(numberofdigits(number))
}

//fun numberOfDigits(number: Int): Any {
//    var counter=1
//    var numberChanged= number
//    return if (number==0) counter
//    else
//}
 fun numberofdigits(number:Int):Int{
    var counter:Int
    if (number != 0){
        counter= 1 + numberofdigits(number/10)
    }
    else{
        return 0
    }
    return counter
 }