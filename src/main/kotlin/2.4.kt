import java.util.Scanner

/*
* AUTHOR: David Moreno Fernández
* DATE: 08/01/2023
* TITLE: Nombres creixents
*/

fun main(){
    val sc= Scanner(System.`in`)
    val numero= sc.nextInt()
    println(checkGrowingNumber(numero))
}

fun checkGrowingNumber(numero:Int):Boolean{
    var grow= true
    if (numero != 0){
        val lastNumber= numero%10
        val previousNumber= numero%100/10
        if (lastNumber > previousNumber){
            val changedNumber= checkGrowingNumber(numero/10)
        }
        else grow= false
    }
    else {
        grow= true
    }
    return grow
}