import java.util.*

/*
* AUTHOR: David Moreno Fernández
* DATE: 08/01/2023
* TITLE: Reducció de dígits
*/
fun main (){
    val sc= Scanner(System.`in`)
    val numero= sc.nextInt()
    var result= reductionOfNumber(numero)
    while (result>9){
       result= reductionOfNumber(result)
    }
    println(result)
}

fun reductionOfNumber (numero:Int):Int{
    var suma:Int
    if (numero != 0){
        suma = numero%10 + reductionOfNumber(numero/10)
    }
    else {
        return 0
    }
    return suma
}

