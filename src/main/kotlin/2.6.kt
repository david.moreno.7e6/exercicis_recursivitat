import java.util.*
import kotlin.collections.ArrayList

/*
* AUTHOR: David Moreno Fernández
* DATE: 08/01/2023
* TITLE: Seqüència d’asteriscos
*/
fun main(){
    val sc= Scanner(System.`in`)
    val numero= sc.nextInt()*2-1
    val numeroInicial= numero
    println(asteriskSequence(numero, numeroInicial))
}
fun asteriskSequence(numero: Int, numeroInicial: Int): Int {
    var result: Int
    if (numero != 0) {
        result = 1 + asteriskSequence(numero - 1, numeroInicial)
        if (result % 2 != 0) {
            println("*")
        }
        else if (result == numeroInicial - 1) {
            repeat(numeroInicial/2+1) {
                print("*")
            }
            println()
        }
        else if (result== numeroInicial/2+1){
            repeat(result-1){
                print("*")
            }
            println()
        }

        else if (result % 2 == 0 && result > numeroInicial / 2) {
            repeat((result - (numeroInicial / 2) + 1).toInt()) {
                print("*")
            }
            println()
        }
        else if (result % 2 == 0 && result <= numeroInicial / 2) {
            repeat(result) {
                print("*")
            }
            println()
        }

    } else {
        return 0
    }
    return result
}

