import java.util.*

/*
* AUTHOR: David Moreno Fernández
* DATE: 11/01/2023
* TITLE: Primers perfectes
*/

fun main() {
    val sc = Scanner(System.`in`)
    val numero = sc.nextInt()
    var divisors=0
    var result= primerPerfecte(numero)
    while (result/10 != 0){
        result= primerPerfecte(result)
    }
    if (result/10 == 0){
        for (i in 1..result){
            if (result%i ==0){
                divisors++
            }
        }
        if (divisors==2){
            println(true)
        }
        else {
            println(false)}
    }
}

fun primerPerfecte(numero:Int):Int{
    var result:Int
    if (numero!=0){
        result= numero%10 + primerPerfecte(numero/10)
    }
    else{
        return 0
    }
    return result
}